<?php
/*
* Wordpress
* Batch adding goods from one category to another (by id category) without deleting from the first category
* The category in which the product is added must be created
* $cat - id of the category with which you want to add goods (row / number), for example: 498
* $cat_to - id of the category in which you want to add goods (row / number), for example: 1201
 */

add_product_cat('498', '1201');

function add_product_cat($cat, $cat_to){
    
    $taxonomy = 'product_cat';
    
    if(!$cat){
        return;
    }else{
        $tags = get_term_by('id', $cat, $taxonomy);
        if($tags == false){
            echo 'The category from which you want to add the product does not exist. Check category id';
            return;
        }else{
            $tags_id = array( $tags->term_id);
        }
    }

    if(!$cat_to){
        return;
    }else{
        $tags_to = get_term_by('id', $cat_to, $taxonomy);
        if($tags == false){
            echo 'The category, in which the product should be added - do not exist. Check category id';
            return;
        }else{
            $tags_id_to = array( $tags_to->term_id);
        }
    }

    $args = array(
            'post_type'=> 'product', 
            'post_status' => 'publish',
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => $taxonomy,
                    'field'    => 'term_id',
                    'terms'    => $tags_id,
                    ),
                ),
            'posts_per_page' => -1,
        );
    
    $loop = new WP_Query( $args );

    while ($loop->have_posts()) {
        $loop->the_post();

        $post_id = $loop->post->ID;

        if(wp_set_post_terms($post_id, $tags_id_to, $taxonomy, true)){
            echo 'ID:'.$post_id.' - the product was added successfully</br>';
        }else{
            echo 'ID:'.$post_id.' - the product failed to be added</br>';
        }
    }
}